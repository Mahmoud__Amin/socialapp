import 'dart:io';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:socialapp/layouts/cubit/states.dart';
import 'package:socialapp/models/message_model.dart';
import 'package:socialapp/models/post_model.dart';
import 'package:socialapp/models/user_model.dart';
import 'package:socialapp/modules/chats/chats_screen.dart';
import 'package:socialapp/modules/feeds/feeds_screen.dart';
import 'package:socialapp/modules/new_posts/new_posts_screen.dart';
import 'package:socialapp/modules/users/users_screen.dart';
import '../../modules/settings/settings_screen.dart';
import '../../shared/components/constans.dart';

class SocialAppCubit extends Cubit<SocialAppStates> {
  SocialAppCubit() : super(SocialAppInitialState());

  static SocialAppCubit get(context) => BlocProvider.of(context);
  List<Widget> screens = [
    FeedsScreen(),
    ChatsScreen(),
    NewPostScreen(),
    UsersScreen(),
    SettingsScreen(),
  ];
  List<String> titles = [
    "Home",
    "Chats",
    "posts",
    "Users",
    "Settings",
  ];
  int currentIndex = 0;

  void changeBottomNavigationBarIndex(int index) {
    if(index==1){
      getUsers();
    }
    if (index == 2) {
      emit(SocialAppNewPostState());
    } else {
      currentIndex = index;
      emit(SocialAppChangeBottomNavigationBarIndexState());
    }
  }

  UserModel? userModel;

  void getUserData() {
    emit(SocialAppLoadingGetUserDataState());
    FirebaseFirestore.instance.collection("users").doc(uId).get().then((value){
      userModel = UserModel.fromJson(value.data()!);
      emit(SocialAppSuccessGetUserDataState());
    }).catchError((error) {
      print(error.toString());
      emit(SocialAppErrorGetUserState(error.toString()));
    });
  }

  File? profileImage;

  var picker = ImagePicker();

  Future getProfileImage() async {
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      profileImage = File(pickedFile.path);
      emit(SocialAppProfileImagePickerSuccessState());
    } else {
      emit(SocialAppProfileImagePickerErrorState());
      print("No image selected");
    }
  }
  File? coverImage;

  Future getCoverImage() async {
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      coverImage = File(pickedFile.path);
      print("path=${pickedFile.path}");
      emit(SocialAppCoverImagePickerSuccessState());
    } else {
      emit(SocialAppCoverImagePickerErrorState());
      print("No image selected");
    }
  }

  void uploadProfileImage({
    required String name,
    required String phone,
    required String bio,
  }){
    emit(SocialAppUserUpdateLoadingState());
    firebase_storage.FirebaseStorage.instance
        .ref()
        .child("users/${Uri.file(profileImage!.path).pathSegments.last}")
        .putFile(profileImage!)
        .then((value) {
        value.ref.getDownloadURL().then((value) {
       // emit(SocialAppUploadProfileImageSuccessState());
        updateUser(
            name: name,
            phone: phone,
            bio: bio,
            image:value,
        );
      }).catchError((error) {
        print('error=${error.toString()}');
        emit(SocialAppUploadProfileImageErrorState());
      });
    }).catchError((error) {
      emit(SocialAppUploadProfileImageErrorState());
      print('error=${error.toString()}');
    });
  }

  void uploadCoverImage({
    required String name,
    required String phone,
    required String bio,
  }){
    emit(SocialAppUserUpdateLoadingState());
    firebase_storage.FirebaseStorage.instance
        .ref()
        .child("users/${Uri.file(profileImage!.path).pathSegments.last}")
        .putFile(coverImage!)
        .then((value) {
        value.ref.getDownloadURL().then((value){
        emit(SocialAppUploadCoverImageSuccessState());
        updateUser(
          name: name,
          phone: phone,
          bio: bio,
          cover:value,
        );
      }).catchError((error) {
        print('error=${error.toString()}');
        emit(SocialAppUploadCoverImageErrorState());
      });
    }).catchError((error) {
      emit(SocialAppUploadCoverImageErrorState());
      print('error=${error.toString()}');
    });
  }

  void updateUser({
    required String name,
    required String phone,
    required String bio,
    String? cover,
    String? image,
  }) {
    UserModel model = UserModel(
      name: name,
      phone: phone,
      bio: bio,
      isEmailVerified: false,
      email: userModel?.email,
      uId:   userModel?.uId,
      image: image ?? userModel?.image,
      cover: cover ?? userModel?.cover,
    );
    print("uId=${userModel?.uId}");
    FirebaseFirestore.instance
        .collection('users')
        .doc(userModel?.uId)
        .update(model.toMap())
        .then((value) {
      emit(SocialAppUserUpdateSuccessState());
      getUserData();
    }).catchError((error) {
      print("error=${error.toString()}");
      emit(SocialAppUserUpdateErrorState());
    });
  }

  File? postImage;

  Future getPostImage() async {
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      postImage = File(pickedFile.path);
      print("path=${pickedFile.path}");
      emit(SocialAppUploadPostImageSuccessState());
    } else {
      emit(SocialAppUploadPostImageErrorState());
      print("No image selected");
    }
  }


  void uploadPostImage({
    required String dateTime,
    required String text,
  }){
    emit(SocialAppCreatePostLoadingState());
    firebase_storage.FirebaseStorage.instance
        .ref()
        .child("posts/${Uri.file(postImage!.path).pathSegments.last}")
        .putFile(postImage!)
        .then((value) {
         value.ref.getDownloadURL().then((value){
         createPost(dateTime:dateTime , text: text,postImage: value);
      }).catchError((error) {
        print('error=${error.toString()}');
        emit(SocialAppUploadCoverImageErrorState());
      });
    }).catchError((error) {
      emit(SocialAppUploadCoverImageErrorState());
      print('error=${error.toString()}');
    });
  }
  void createPost({
    required String dateTime,
    required String text,
    String? postImage,

  }){
    emit(SocialAppCreatePostLoadingState());
    PostModel model =PostModel(
      name: userModel?.name,
      image: userModel?.image,
      uId: userModel?.uId,
      dateTime: dateTime,
      text: text,
      postImage: postImage??''
    );
    FirebaseFirestore.instance
    .collection('posts')
    .add(model.toMap())
    .then((value){
      emit(SocialAppCreatePostSuccessState());
    }).catchError((error){
      emit(SocialAppCreatePostErrorState());
    });
  }
  void removePostImage(){
    postImage=null;
    emit(SocialAppRemovePostImageState());
  }
  List<PostModel> posts=[];
  List<String>    postId=[];
  List<int>  likes=[];
  void getPosts(){
   FirebaseFirestore.instance
       .collection('posts')
       .get()
       .then((value){
         value.docs.forEach((element) {
           element.reference.collection('likes')
           .get()
           .then((value){
               likes.add(value.docs.length);
               postId.add(element.id);
               posts.add(PostModel.fromJson(element.data()));
               
           })
           .catchError((error){
             
           });

         });
         emit(SocialAppSuccessGetPostsState());
   }).catchError((error){
       emit(SocialAppErrorGetPostsState(error));
   });
  }

  void likePost(String postId){
    FirebaseFirestore.instance
        .collection('posts')
        .doc(postId)
        .collection('likes')
        .doc(userModel?.uId)
        .set({
          'like':true,
         })
        .then((value){
          emit(SocialAppLikePostSuccessState());
         })
        .catchError((error){
          emit(SocialAppLikePostErrorState(error.toString()));
    });
  }
 List<UserModel>  users=[];
 void getUsers(){

   if(users.length==0)
     FirebaseFirestore.instance
       .collection('users')
       .get()
       .then((value){
         value.docs.forEach((element) {
           if(element.data()['uId']!=userModel?.uId)
             users.add(UserModel.fromJson(element.data()));
           });
         emit(SocialAppSuccessGetAllUsersState());
       }).catchError((error){
     emit(SocialAppErrorGetAllUsersState(error));
   });
 }

 void sendMessage({
    required String receiverId,
    required String dateTime,
    required String text,
  })
 {
   MessageModel model =MessageModel(
     text: text,
     senderId:userModel!.uId,
     receiver: receiverId,
     dateTime: dateTime
   );
   // set my chat
    FirebaseFirestore.instance
   .collection('users')
   .doc(userModel?.uId)
   .collection("chats")
   .doc(receiverId)
   .collection('messages')
   .add(model.toMap())
   .then((value){
      emit(SocialAppSendMessagesSuccessState());
    })
   .catchError((error){
      emit(SocialAppSendMessagesErrorState());
    });
    // set receiver chat
   FirebaseFirestore.instance
       .collection('users')
       .doc(receiverId)
       .collection("chats")
       .doc(userModel?.uId)
       .collection('messages')
       .add(model.toMap())
       .then((value){
     emit(SocialAppSendMessagesSuccessState());
   })
       .catchError((error){
     emit(SocialAppSendMessagesErrorState());
   });
 }
 List<MessageModel> messages=[];
 void getMessages({required String receiverId}){

   FirebaseFirestore.instance
       .collection('users')
       .doc(userModel?.uId)
       .collection('chats')
       .doc(receiverId)
       .collection('messages').
        orderBy('dateTime')
       .snapshots()
       .listen((event){
         messages=[];
         event.docs.forEach((element) {
           messages.add(MessageModel.fromJson(element.data()));
         });
             emit(SocialAppGetMessagesSuccessState());
   });
 }
}

