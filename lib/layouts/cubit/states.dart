

abstract class SocialAppStates {}

class SocialAppInitialState extends SocialAppStates{}

class SocialAppChangeBottomNavigationBarIndexState extends SocialAppStates{}

class SocialAppLoadingGetUserDataState extends SocialAppStates{}

class SocialAppSuccessGetUserDataState extends SocialAppStates{}

class SocialAppErrorGetUserState extends SocialAppStates{
   final String error;
   SocialAppErrorGetUserState(this.error);
}

class SocialAppLoadingGetAllUsersState extends SocialAppStates{}

class SocialAppSuccessGetAllUsersState extends SocialAppStates{}

class SocialAppErrorGetAllUsersState extends SocialAppStates{
   final String error;
   SocialAppErrorGetAllUsersState(this.error);
}

class SocialAppLoadingGetPostsState extends SocialAppStates{}

class SocialAppSuccessGetPostsState extends SocialAppStates{}

class SocialAppErrorGetPostsState extends SocialAppStates{
   final String error;
   SocialAppErrorGetPostsState(this.error);
}

class SocialAppLikePostLoadingState extends SocialAppStates{}

class SocialAppLikePostSuccessState extends SocialAppStates{}

class SocialAppLikePostErrorState extends SocialAppStates{
   final String error;
   SocialAppLikePostErrorState(this.error);
}
class SocialAppLoadingEditUserDataState extends SocialAppStates{}

class SocialAppSuccessEditUserDataState extends SocialAppStates{}

class SocialAppErrorEditUserState extends SocialAppStates{
   final String error;
   SocialAppErrorEditUserState(this.error);
}
class SocialAppNewPostState extends SocialAppStates{}

class SocialAppProfileImagePickerSuccessState extends SocialAppStates{}

class SocialAppProfileImagePickerErrorState extends SocialAppStates{

}

class SocialAppCoverImagePickerSuccessState extends SocialAppStates{}

class SocialAppCoverImagePickerErrorState extends SocialAppStates{}



class SocialAppUploadProfileImageSuccessState extends SocialAppStates{}

class SocialAppUploadProfileImageErrorState extends SocialAppStates{}



class SocialAppUploadCoverImageSuccessState extends SocialAppStates{}

class SocialAppUploadCoverImageErrorState extends SocialAppStates{}


class SocialAppUserUpdateLoadingState extends SocialAppStates{}

class SocialAppUserUpdateSuccessState extends SocialAppStates{}

class SocialAppUserUpdateErrorState extends SocialAppStates{}



class SocialAppCreatePostLoadingState extends SocialAppStates{}

class SocialAppCreatePostSuccessState extends SocialAppStates{}

class SocialAppCreatePostErrorState extends SocialAppStates{}



class SocialAppUploadPostImageSuccessState extends SocialAppStates{}

class SocialAppUploadPostImageErrorState extends SocialAppStates{}

class SocialAppRemovePostImageState extends SocialAppStates{}

class SocialAppSendMessagesSuccessState extends SocialAppStates{}

class SocialAppSendMessagesErrorState extends SocialAppStates{}

class SocialAppGetMessagesSuccessState extends SocialAppStates{}

class SocialAppGetMessagesErrorState extends SocialAppStates{}