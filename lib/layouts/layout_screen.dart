import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialapp/modules/new_posts/new_posts_screen.dart';
import '../shared/components/components.dart';
import 'cubit/cubit.dart';
import 'cubit/states.dart';

class LayoutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SocialAppCubit, SocialAppStates>(
      listener: (BuildContext context, state) {
        if(state is SocialAppNewPostState){
          navigateTo(context, NewPostScreen()) ;
        }
      },
      builder: (BuildContext context, Object? state) {
        var cubit = SocialAppCubit.get(context);
        return Scaffold(
          appBar: AppBar(
            title: Text(cubit.titles[cubit.currentIndex]),
            actions: [
              IconButton(onPressed: (){}, icon: const Icon(Icons.notifications_none_outlined)),
              IconButton(onPressed: (){}, icon: const Icon(Icons.search)),
            ],
          ),
          body: ConditionalBuilder(
            builder: (BuildContext context) {
              var model = SocialAppCubit.get(context).userModel;
              print("cubit.current index=${cubit.currentIndex}");
              return cubit.screens[cubit.currentIndex];
            },
            fallback: (BuildContext context) =>
                const Center(child: CircularProgressIndicator()),
            condition: SocialAppCubit.get(context).userModel != null,
          ),
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: cubit.currentIndex,
            onTap: (index) {
              cubit.changeBottomNavigationBarIndex(index);
              print(index);
            },
            items:  const [
              BottomNavigationBarItem(label: 'Home', icon: Icon(Icons.home)),
              BottomNavigationBarItem(label: 'Chats', icon: Icon(Icons.chat_bubble)),
              BottomNavigationBarItem(label: 'Posts', icon: Icon(Icons.upload_file)),
              BottomNavigationBarItem(label: 'Users', icon: Icon(Icons.location_on_outlined)),
              BottomNavigationBarItem(label: 'Settings', icon: Icon(Icons.settings)),

            ],
          ),
        );
      },
    );
  }
}
