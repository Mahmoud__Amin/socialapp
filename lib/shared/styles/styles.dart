import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:socialapp/shared/styles/color.dart';

ThemeData lightTheme= ThemeData(
    appBarTheme: const AppBarTheme(
        color: Colors.white,
        titleSpacing: 20,
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Colors.white,
            statusBarIconBrightness: Brightness.dark),
            titleTextStyle: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Colors.black),
        iconTheme: IconThemeData(color: Colors.black)),
    primaryColor:defaultColor,
    bottomNavigationBarTheme: const BottomNavigationBarThemeData(
      type: BottomNavigationBarType.fixed,
      selectedItemColor: Colors.blue,
      unselectedItemColor: Colors.grey,
    ),
    scaffoldBackgroundColor: Colors.white,
    textTheme: const TextTheme(
        bodyText1: TextStyle(
            color: Colors.black,
            fontSize: 18,
            fontWeight: FontWeight.w600),
            subtitle1: TextStyle(
            color: Colors.black,
            fontSize: 14,
            fontWeight: FontWeight.w600,
            height: 1.3),
    ),
    backgroundColor: Colors.white,
    );

ThemeData darkTheme=ThemeData(
    appBarTheme: AppBarTheme(
        titleSpacing: 20,
        color: HexColor('333739'),
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: HexColor('333739'),
            statusBarIconBrightness: Brightness.light),
        titleTextStyle: const TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Colors.white),
        iconTheme: const IconThemeData(color: Colors.white)),
    progressIndicatorTheme:
    const ProgressIndicatorThemeData(color: Colors.deepOrange),
    inputDecorationTheme: const InputDecorationTheme(
      labelStyle: TextStyle(color: Colors.deepOrange),
      prefixIconColor: Colors.deepOrange,
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.deepOrange),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.deepOrange),
      ),
    ),
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
        backgroundColor: Colors.deepOrangeAccent),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      type: BottomNavigationBarType.fixed,
      backgroundColor: HexColor('333739'),
      selectedItemColor: Colors.deepOrangeAccent,
      unselectedItemColor: Colors.grey,
    ),
    textTheme: const TextTheme(
        bodyText1: TextStyle(
            color: Colors.white,
            fontSize: 18,
            fontWeight: FontWeight.w600),
        subtitle1: TextStyle(
          color: Colors.white,
          fontSize: 14,
          fontWeight: FontWeight.w600,
          height: 1.3
        ),
    ),
    scaffoldBackgroundColor: HexColor('333739'),
    backgroundColor: Colors.white);
