
import '../../modules/login/login_screen.dart';
import '../network/local/cache_helper.dart';
import 'components.dart';

void logOut(context){
  CacheHelper.removeData(key:"token");
  navigateReplace(context, LoginScreen());
}
void printFullText(String? text){
  final pattern = RegExp('.{1,8000}');
  pattern.allMatches(text!).forEach((element)=>print(element.group(0)));
}
String? token="";
String? uId="";