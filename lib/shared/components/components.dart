import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:socialapp/layouts/cubit/cubit.dart';

import 'package:socialapp/modules/on_boarding/on_boarding_screen.dart';

import '../styles/color.dart';

Widget buildBoardingItem(BuildContext context, PageBoardingModels model) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Image.asset(model.image),
      const SizedBox(height: 100),
      Text(model.title, style: Theme.of(context).textTheme.bodyText1),
      const SizedBox(height: 30),
      Text(model.body),
    ],
  );
}

Widget defaultFromField({
  required TextEditingController controller,
  required TextInputType type,
  void Function(String value)? onSubmit,
  void Function(String value)? onChange,
  void Function()? onTap,
  bool isPassword = false,
  required String? Function(String? value) validate,
  required String label,
  required IconData prefix,
  IconData? suffix,
  void Function()? suffixPressed,
  bool isClickable = true,
}) =>
    TextFormField(
      controller: controller,
      keyboardType: type,
      obscureText: isPassword,
      validator: validate,
      onChanged: onChange,
      onTap: onTap,
      readOnly: !isClickable,
      onFieldSubmitted: onSubmit,
      decoration: InputDecoration(
          label: Text(label),
          prefixIcon: Icon(prefix),
          suffixIcon: IconButton(onPressed: suffixPressed, icon: Icon(suffix)),
          border: const OutlineInputBorder()),
    );

Widget defaultButton({
  double width = double.infinity,
  Color background = Colors.blue,
  required void Function() function,
  required String text,
  double radius = 0.0,
  bool isUpperCase = true,
}) =>
    Container(
      width: width,
      height: 40.0,
      child: MaterialButton(
        onPressed: function,
        child: Text(
          isUpperCase ? text.toUpperCase() : text,
          style: const TextStyle(
            color: Colors.white,
          ),
        ),
      ),
      decoration: BoxDecoration(
          color: background, borderRadius: BorderRadius.circular(radius)),
    );

Widget defaultTextButton({
  required void Function() function,
  required String text,
}) =>
    TextButton(onPressed: function, child: Text(text.toUpperCase()));

void navigateTo(BuildContext context, Widget widget) {
  Navigator.push(
      context, MaterialPageRoute(builder: (BuildContext context) => widget));
}

void navigateReplace(context, Widget widget) {
  Navigator.pushReplacement(
      context, MaterialPageRoute(builder: (BuildContext context) => widget));
}

void defaultToast({
  required String message,
  required ToastStates state,
}) {
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      backgroundColor: choseToastColor(state),
      textColor: Colors.white,
      fontSize: 16.0);
}

enum ToastStates { SUCCESS, ERROR, WARNING }

Color choseToastColor(ToastStates toastStates) {
  if (toastStates == ToastStates.SUCCESS)
    return Colors.green;
  else if (toastStates == ToastStates.ERROR) return Colors.red;
  return Colors.amber;
}

Widget divider() => Padding(
      padding: const EdgeInsetsDirectional.only(
        start: 20.0,
      ),
      child: Container(
        width: double.infinity,
        height: 1.0,
        color: Colors.grey[300],
      ),
    );


PreferredSizeWidget defaultAppBar({
  required BuildContext context,
  String? title,
  List<Widget>?actions
})=>AppBar(
    leading: IconButton(
      onPressed: () {
        Navigator.pop(context);
      },
      icon: Icon(Icons.arrow_back_ios_new_outlined),
    ),
    titleSpacing: 5.0,
    title: Text(title!),
    actions: actions,

);