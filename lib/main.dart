import 'package:bloc/bloc.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialapp/shared/blocobserver.dart';
import 'package:socialapp/shared/components/components.dart';
import 'package:socialapp/shared/components/constans.dart';
import 'package:socialapp/shared/network/local/cache_helper.dart';
import 'package:socialapp/shared/network/remote/dio_helper.dart';
import 'package:socialapp/shared/styles/styles.dart';
import 'layouts/cubit/cubit.dart';
import 'layouts/layout_screen.dart';
import 'modules/login/cubit/cubit.dart';
import 'modules/login/login_screen.dart';
import 'modules/on_boarding/on_boarding_screen.dart';
import 'modules/register/cubit/cubit.dart';

Future<void> fireBaseMessagingBackgroundHandler(RemoteMessage message) async{
   print("On background message");
   print(message.data.toString());
   defaultToast(message: "On background message", state: ToastStates.SUCCESS);

}
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  var token =await FirebaseMessaging.instance.getToken();
  print("token=$token");
  FirebaseMessaging.onMessage.listen((event){
    print("On message");
    print(event.data.toString());
    defaultToast(message:"On message" , state: ToastStates.SUCCESS);
  });
  FirebaseMessaging.onMessageOpenedApp.listen((event) {
    print("On message opened app");
    print(event.data.toString());
    defaultToast(message:"On message opened app" , state: ToastStates.SUCCESS);
  });
  FirebaseMessaging.onBackgroundMessage((message) => fireBaseMessagingBackgroundHandler(message));
  await CacheHelper.init();
  //bool? isDark =CacheHelper.getdata(key:"isDark");
  bool? OnBoarding=CacheHelper.getDate(key:'OnBoarding');
  uId=CacheHelper.getDate(key:'uId');
  print("uId from the main=$uId");
  Widget widget=OnBoardingScreen();
  if(uId!=null){
    widget=LayoutScreen();
  }
  else if(OnBoarding!=null){
    widget=LoginScreen();
  }
  DioHelper.init();
  BlocOverrides.runZoned(
        ()  => {
      runApp(MyApp(widget)),
    },
    blocObserver: MyBlocObserver(),
  );
}

class MyApp extends StatelessWidget {
  final Widget widget;
   MyApp(this.widget);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (BuildContext context) => LoginCubit(),),
        BlocProvider(
          create: (BuildContext context) => RegisterCubit(),),
        BlocProvider(create: (BuildContext context) => SocialAppCubit()..getUserData()..getPosts())
      ],
      child: MaterialApp(
        theme: lightTheme,
        darkTheme: darkTheme,
        themeMode: ThemeMode.light,
        debugShowCheckedModeBanner: false,
        home: widget
      ),
    );
  }
}

