class MessageModel {
  String? senderId;
  String? receiver;
  String? dateTime;
  String? text;
  MessageModel(
      {
        this.senderId,
        this.receiver,
        this.dateTime,
        this.text,
      });

  MessageModel.fromJson(Map<String, dynamic> json) {
    senderId = json['senderId'];
    receiver = json['receiver'];
    dateTime = json['dateTime'];
    text = json['text'];

  }
  Map<String, dynamic> toMap() {
    return {
      'senderId': senderId,
      'receiver': receiver,
      'dateTime': dateTime,
      'text': text,
    };
  }
}
