import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialapp/models/user_model.dart';
import 'package:socialapp/modules/register/cubit/states.dart';

class RegisterCubit extends Cubit<RegisterStates> {
  RegisterCubit() : super(RegisterInitialState());

  static RegisterCubit get(context) => BlocProvider.of(context);
  bool passwordIsShown = true;

  void changeLoginVisibilityIcon() {
    passwordIsShown = !passwordIsShown;
    emit(RegisterChangePasswordVisibilityState());
  }

  void userRegister({
    required String email,
    required String password,
    required String name,
    required String phone,
  }) {
    emit(RegisterLoadingState());
    FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password
    ).then((value) {
      userCreate(name: name, email: email, phone: phone, uId: value.user!.uid);
      print(value.user?.email.toString());
      print(value.user?.uid.toString());
    }).catchError((Error) {
      emit(RegisterErrorState(Error.toString()));
    });
  }
  void userCreate({
    required String name,
    required String email,
    required String phone,
    required String uId,
  }) {
     UserModel userModel=UserModel(
       name: name,
       email: email,
       phone: phone,
       uId: uId,
       bio: "write your bio....",
       cover: "https://media.istockphoto.com/id/945088692/nl/foto/opgewonden-man-in-geel-hand-in-hand-omhoog.webp?s=612x612&w=is&k=20&c=UFTmsQyrk9dXuTy8IAmCo3jN9SiIFUwNhQu0OOwUGoQ=",
       image: "https://media.istockphoto.com/id/945088692/nl/foto/opgewonden-man-in-geel-hand-in-hand-omhoog.webp?s=612x612&w=is&k=20&c=UFTmsQyrk9dXuTy8IAmCo3jN9SiIFUwNhQu0OOwUGoQ=",
       isEmailVerified: false
     );
     FirebaseFirestore.instance.collection("users").doc(uId).set(
       userModel.toMap()
     ).then((value){
       emit(CreateUserSuccessState());
     }).catchError((Error){
       print(Error.toString());
      emit(CreateUserErrorState(Error));
     });
  }
/*DioHelper.postData(
      url:REGISTER,
      data:{
        'email':email,
        'password':password,
        'name':name,
        'phone':phone,
        'image':"https://student.valuxapps.com/storage/uploads/categories/16301438353uCFh.29118.jpg"
    },
    )?.then((value){
       registerModel=LoginModel.fromJson(value?.data);
       emit(RegisterSuccessState(registerModel!));
    }).catchError((error){
       emit(RegisterErrorState(error.toString()));
       print(error.toString());
    });*/
}
