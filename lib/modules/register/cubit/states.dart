

abstract class RegisterStates{}

class RegisterInitialState extends RegisterStates {}

class RegisterLoadingState extends RegisterStates {}

class RegisterSuccessState extends RegisterStates {}

class RegisterErrorState extends RegisterStates {
  final String Error;
  RegisterErrorState(this.Error);
}

class CreateUserSuccessState extends RegisterStates {}

class CreateUserErrorState extends RegisterStates {
  final String Error;
  CreateUserErrorState(this.Error);
}

class RegisterChangePasswordVisibilityState extends RegisterStates {}