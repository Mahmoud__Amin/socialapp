import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialapp/modules/register/cubit/cubit.dart';
import '../../layouts/layout_screen.dart';
import '../../shared/components/components.dart';
import 'cubit/states.dart';
class RegisterScreen extends StatelessWidget {
  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  var nameController = TextEditingController();
  var phoneController = TextEditingController();
  var formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<RegisterCubit, RegisterStates>(
        listener: (BuildContext context, state) {
           if(state is CreateUserSuccessState){
            navigateReplace(context, LayoutScreen());
          }
        },
        builder: (BuildContext context, state) {
          return Scaffold(
            appBar: AppBar(),
            body: Center(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Form(
                    key: formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Register".toUpperCase(),
                          style: Theme
                              .of(context)
                              .textTheme
                              .headline4
                              ?.copyWith(color: Colors.black),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Text(
                          "Register now to communicate with friends",
                          style: Theme
                              .of(context)
                              .textTheme
                              .bodyText1
                              ?.copyWith(color: Colors.grey),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        defaultFromField(
                            controller: nameController,
                            type: TextInputType.name,
                            validate: (String? value) {
                              if (value!.isEmpty) {
                                return "Name must not be empty";
                              }
                              return null;
                            },
                            label: "User Name",
                            prefix: Icons.person),
                        const SizedBox(
                          height: 15,
                        ),
                        defaultFromField(
                            controller: emailController,
                            type: TextInputType.emailAddress,
                            validate: (String? value) {
                              if (value!.isEmpty) {
                                return "Email Addresses must not be empty";
                              }
                              return null;
                            },
                            label: "Email Address",
                            prefix: Icons.email_outlined),
                        const SizedBox(
                          height: 15,
                        ),
                        defaultFromField(
                            controller: passwordController,
                            type: TextInputType.visiblePassword,
                            validate: (String? value) {
                              if (value!.isEmpty) {
                                return "Password must not be empty";
                              }
                              return null;
                            },
                            label: "Password",
                            isPassword: RegisterCubit
                                .get(context)
                                .passwordIsShown,
                            suffix: RegisterCubit
                                .get(context)
                                .passwordIsShown
                                ? Icons.visibility_off_outlined
                                : Icons.visibility_outlined,
                            suffixPressed: () =>
                                RegisterCubit.get(context)
                                    .changeLoginVisibilityIcon(),
                            prefix: Icons.lock_outline),
                        const SizedBox(
                          height: 15,
                        ),
                        defaultFromField(
                            controller: phoneController,
                            type: TextInputType.phone,
                            validate: (String? value) {
                              if (value!.isEmpty) {
                                return "phone must not be empty";
                              }
                              return null;
                            },
                            label: "Phone",
                            prefix: Icons.phone),
                        const SizedBox(
                          height: 25,
                        ),
                        ConditionalBuilder(
                          condition: (state is! RegisterLoadingState),
                          builder: (context) =>
                              defaultButton(
                                text: "Register",
                                function: () {
                                  if (formKey.currentState!.validate()) {
                                    RegisterCubit.get(context).userRegister(
                                        email: emailController.text,
                                        password: passwordController.text,
                                        name: nameController.text,
                                        phone: phoneController.text
                                    );
                                  }
                                },
                              ),
                          fallback: (context) =>
                              const Center(child: CircularProgressIndicator()),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        }

    );
  }
}
