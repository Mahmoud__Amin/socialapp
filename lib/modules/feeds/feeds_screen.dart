import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialapp/layouts/cubit/states.dart';
import 'package:socialapp/shared/styles/color.dart';
import '../../layouts/cubit/cubit.dart';
import '../../models/post_model.dart';

class FeedsScreen extends StatelessWidget {
  const FeedsScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (BuildContext context) {
        SocialAppCubit.get(context).getPosts();
        return BlocConsumer<SocialAppCubit, SocialAppStates>(
          listener: (BuildContext context, state) {},
          builder: (BuildContext context, Object? state) {
            print("posts=${SocialAppCubit.get(context).posts.toString()}");
            print(
                "usermodel=${SocialAppCubit.get(context).userModel.toString()}");
            return ConditionalBuilder(
              builder: (BuildContext context) {
                return SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Column(
                    children: [
                      Card(
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        elevation: 6,
                        margin: EdgeInsets.all(8.0),
                        child: Stack(
                          alignment: AlignmentDirectional.bottomEnd,
                          children: [
                            Image(
                              image: NetworkImage(
                                  "https://img.freepik.com/free-photo/friendly-smiling-young-woman-with-brown-hair-gives-good-advice-suggestion-what-buy-indicates-with-fore-finger-upper-right-corner_273609-18601.jpg?t=st=1662192725~exp=1662193325~hmac=e8172dc2677a9e855f232e6f36ed96d9858fc03e0051d955a706efde0b7d96f6"),
                              fit: BoxFit.cover,
                              height: 200,
                              width: double.infinity,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "Communicate with friends",
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1
                                    ?.copyWith(
                                        color: Colors.white, fontSize: 14),
                              ),
                            )
                          ],
                        ),
                      ),
                      ListView.separated(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (BuildContext context, int index) {
                          return buildPostItem(
                              SocialAppCubit.get(context).posts[index],
                              context,
                              index);
                        },
                        separatorBuilder: (BuildContext context, int index) =>
                            SizedBox(
                          height: 8,
                        ),
                        itemCount: SocialAppCubit.get(context).posts.length,
                      ),
                      SizedBox(
                        height: 8.0,
                      )
                    ],
                  ),
                );
              },
              fallback: (BuildContext context) {
                return const Center(child: CircularProgressIndicator());
              },
              condition: SocialAppCubit.get(context).posts.isNotEmpty &&
                  SocialAppCubit.get(context).userModel != null,
            );
          },
        );
      },
    );
  }

  Widget buildPostItem(PostModel model, context, index) => Card(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        elevation: 5,
        margin: EdgeInsets.symmetric(
          horizontal: 8.0,
        ),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  CircleAvatar(
                    radius: 20,
                    backgroundImage: NetworkImage("${model.image}"),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Expanded(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "${model.name}",
                            style: TextStyle(height: 1.4),
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                          Icon(
                            Icons.check_circle,
                            color: defaultColor,
                            size: 16.0,
                          )
                        ],
                      ),
                      Text(
                        "${model.dateTime}",
                        style: Theme.of(context)
                            .textTheme
                            .caption
                            ?.copyWith(height: 1.4),
                      )
                    ],
                  )),
                  SizedBox(
                    width: 15,
                  ),
                  IconButton(
                      onPressed: () {},
                      icon: Icon(
                        Icons.more_horiz,
                        size: 16,
                      ))
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 15.0),
                child: Container(
                  width: double.infinity,
                  height: 1,
                  color: Colors.grey[300],
                ),
              ),
              Text(
                "${model.text}",
                style: Theme.of(context).textTheme.subtitle1,
              ),
              /*Padding(
                padding: const EdgeInsets.only(bottom: 10, top: 5),
                child: Container(
                  width: double.infinity,
                  child: Wrap(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 6),
                        child: Container(
                          height: 20,
                          child: MaterialButton(
                              onPressed: () {},
                              minWidth: 1.0,
                              padding: EdgeInsets.zero,
                              child: Text(
                                "#software",
                                style: Theme.of(context)
                                    .textTheme
                                    .caption
                                    ?.copyWith(color: defaultColor),
                              )),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 6),
                        child: Container(
                          height: 20,
                          child: MaterialButton(
                              onPressed: () {},
                              minWidth: 1.0,
                              padding: EdgeInsets.zero,
                              child: Text(
                                "#Flutter",
                                style: Theme.of(context)
                                    .textTheme
                                    .caption
                                    ?.copyWith(color: defaultColor),
                              )),
                        ),
                      ),
                    ],
                  ),
                ),
              ),*/
              if (model.postImage != '')
                Padding(
                  padding: const EdgeInsetsDirectional.only(top: 15),
                  child: Container(
                      height: 140,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        image: DecorationImage(
                          image: NetworkImage("${model.postImage}"),
                          fit: BoxFit.cover,
                        ),
                      )),
                ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Row(
                  children: [
                    Expanded(
                      child: InkWell(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          child: Row(
                            children: [
                              Icon(
                                Icons.favorite_border,
                                size: 16,
                                color: Colors.red,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Text(
                                "${SocialAppCubit.get(context).likes[index]}",
                                style: Theme.of(context).textTheme.caption,
                              )
                            ],
                          ),
                        ),
                        onTap: () {},
                      ),
                    ),
                    Expanded(
                      child: InkWell(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Icon(
                                Icons.chat_bubble_outline_outlined,
                                size: 16,
                                color: Colors.amber,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Text(
                                "0 comment",
                                style: Theme.of(context).textTheme.caption,
                              )
                            ],
                          ),
                        ),
                        onTap: () {},
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Container(
                  width: double.infinity,
                  height: 1,
                  color: Colors.grey[300],
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: InkWell(
                      child: Row(
                        children: [
                          CircleAvatar(
                            radius: 18,
                            backgroundImage: NetworkImage(
                                "${SocialAppCubit.get(context).userModel?.image}"),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          /*TextFormField(
                            initialValue:"write a comment ...",
                            style: TextStyle(height: 1.4),
                          )*/
                          Text(
                            "write a comment ...",
                            style: TextStyle(height: 1.4),
                          ),
                        ],
                      ),
                      onTap: () {},
                    ),
                  ),
                  InkWell(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Icon(
                            Icons.favorite_border,
                            size: 16,
                            color: Colors.red,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "Like",
                            style: Theme.of(context).textTheme.caption,
                          )
                        ],
                      ),
                    ),
                    onTap: () {
                      SocialAppCubit.get(context)
                          .likePost(SocialAppCubit.get(context).postId[index]);
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      );
}
