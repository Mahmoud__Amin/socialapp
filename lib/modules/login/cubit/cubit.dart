
import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialapp/modules/login/cubit/states.dart';



class LoginCubit extends Cubit<LoginStates>{
  LoginCubit() : super(LoginInitialState());
  static LoginCubit get(context)=>BlocProvider.of(context);
  bool passwordIsShown=true;
  void changeLoginVisibilityIcon(){
       passwordIsShown=!passwordIsShown;
       emit(LoginChangePasswordVisibilityState());
  }

  void userLogin({
     required String email,
     required String password
}){
    emit(LoginLoadingState());
    FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: password).then((value){
      emit(LoginSuccessState(value.user!.uid));
      print(value.user!.email);
      print(value.user!.uid);

    }).catchError((error){
      emit(LoginErrorState(error.toString()));
      print(error.toString());
    });

    /*DioHelper.postData(
      url:LOGIN,
      data:{
        "email":email,
        "password":password,
    },
    )?.then((value){

       loginModel=LoginModel.fromJson(value?.data);
       emit(LoginSuccessState(loginModel!));
    }).catchError((error){
       emit(LoginErrorState(error.toString()));
       print(error.toString());
    });*/
  }

}