
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialapp/layouts/layout_screen.dart';
import 'package:socialapp/modules/register/register_screen.dart';
import 'package:socialapp/shared/components/components.dart';
import 'package:socialapp/shared/network/local/cache_helper.dart';
import 'cubit/cubit.dart';
import 'cubit/states.dart';
class LoginScreen extends StatelessWidget {
  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  var formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LoginCubit, LoginStates>(
        listener: (BuildContext context, state) {
        if (state is LoginErrorState) {
          defaultToast(message: state.error, state: ToastStates.ERROR);
        }
        else if(state is LoginSuccessState){
              CacheHelper.setData(key: 'uId', value:state.uId ).then((value){
              navigateReplace(context, LayoutScreen());
            });

        }
      },
      builder: (BuildContext context, state) {
        return Scaffold(
          appBar: AppBar(),
          body: Center(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Form(
                  key: formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Login".toUpperCase(),
                        style: Theme
                            .of(context)
                            .textTheme
                            .headline4
                            ?.copyWith(color: Colors.black),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Text(
                        "Login now to communicate with friends",
                        style: Theme
                            .of(context)
                            .textTheme
                            .bodyText1
                            ?.copyWith(color: Colors.grey),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      defaultFromField(
                          controller: emailController,
                          type: TextInputType.emailAddress,
                          validate: (String? value) {
                            if (value!.isEmpty) {
                              return "Email Addresses must not be empty";
                            }
                            return null;
                          },
                          label: "Email Address",
                          prefix: Icons.email_outlined),
                      const SizedBox(
                        height: 15,
                      ),
                      defaultFromField(
                          controller: passwordController,
                          type: TextInputType.visiblePassword,
                          validate: (String? value) {
                            if (value!.isEmpty) {
                              return "Password must not be empty";
                            }
                            return null;
                          },
                          label: "Password",
                          isPassword: LoginCubit
                              .get(context)
                              .passwordIsShown,
                          onSubmit: (value) {
                            if (formKey.currentState!.validate()) {
                              LoginCubit.get(context).userLogin(
                                  email: emailController.text,
                                  password: passwordController.text);
                            }
                          },
                          suffix: LoginCubit
                              .get(context)
                              .passwordIsShown
                              ? Icons.visibility_off_outlined
                              : Icons.visibility_outlined,
                          suffixPressed: () =>
                              LoginCubit.get(context)
                                  .changeLoginVisibilityIcon(),
                          prefix: Icons.lock_outline),
                      const SizedBox(
                        height: 25,
                      ),
                      ConditionalBuilder(
                        condition: (state is! LoginLoadingState),
                        builder: (context) =>
                            defaultButton(
                              text: "login",
                              function: () {
                                if (formKey.currentState!.validate()) {
                                  LoginCubit.get(context).userLogin(
                                      email: emailController.text,
                                      password: passwordController.text);
                                }
                              },
                            ),
                        fallback: (context) =>
                            const Center(child: CircularProgressIndicator()),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Text("Don't have an account?"),
                            defaultTextButton(
                                function: () {
                                  navigateTo(
                                      context, RegisterScreen());
                                },
                                text: "register")
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      }

    );
  }
}
