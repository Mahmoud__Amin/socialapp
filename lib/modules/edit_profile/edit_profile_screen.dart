
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialapp/layouts/cubit/cubit.dart';
import 'package:socialapp/layouts/cubit/states.dart';
import 'package:socialapp/shared/components/components.dart';

class EditProfileScreen extends StatelessWidget {

  var nameController = TextEditingController();
  var bioController = TextEditingController();
  var phoneController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SocialAppCubit, SocialAppStates>(
      listener: (BuildContext context, state) {},
      builder: (BuildContext context, Object? state) {
        var userModel = SocialAppCubit.get(context).userModel;
        var profileImage = SocialAppCubit.get(context).profileImage;
        var coverImage = SocialAppCubit.get(context).coverImage;
        nameController.text=userModel!.name!;
        bioController.text=userModel.bio!;
        phoneController.text=userModel.phone!;


        return Scaffold(
          appBar:
              defaultAppBar(context: context, title: "Edit Profile", actions: [
              defaultTextButton(function: () {
              SocialAppCubit.get(context).updateUser(name: nameController.text, phone: phoneController.text,bio: bioController.text);
            }, text: "Update"),
            const SizedBox(
              width: 15,
            )
          ]),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  if(state is SocialAppUserUpdateLoadingState)LinearProgressIndicator(),
                  if(state is SocialAppUserUpdateLoadingState)SizedBox(height: 10,),
                  Container(
                    height: 190,
                    child: Stack(
                      alignment: AlignmentDirectional.bottomCenter,
                      children: [
                        Align(
                          child: Stack(
                            alignment: AlignmentDirectional.topEnd,
                            children: [
                              Container(
                                  height: 140,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(4.0),
                                      topRight: Radius.circular(4.0),
                                    ),
                                    image: DecorationImage(
                                      image: (coverImage==null)?NetworkImage("${userModel.cover}"):Image.file(coverImage).image,
                                      fit: BoxFit.cover,
                                    ),
                                  )),
                              IconButton(
                                  onPressed: () {
                                    SocialAppCubit.get(context).getCoverImage();
                                  },
                                  icon: CircleAvatar(
                                      radius: 20.0,
                                      child: Icon(
                                        Icons.camera_alt_outlined,
                                        size: 16,
                                      )))
                            ],
                          ),
                          alignment: AlignmentDirectional.topCenter,
                        ),
                        Stack(
                          alignment: AlignmentDirectional.bottomEnd,
                          children: [
                            CircleAvatar(
                              radius: 64,
                              backgroundColor:
                                  Theme.of(context).scaffoldBackgroundColor,
                              child: CircleAvatar(
                                  radius: 60,
                                  backgroundImage: (profileImage==null)?NetworkImage("${userModel.image}"):Image.file(profileImage).image),
                            ),
                            IconButton(
                                onPressed: () {
                                  SocialAppCubit.get(context).getProfileImage();
                                },
                                icon: CircleAvatar(
                                    radius: 20.0,
                                    child: Icon(
                                      Icons.camera_alt_outlined,
                                      size: 16,
                                    )))
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  if(SocialAppCubit.get(context).profileImage!=null||SocialAppCubit.get(context).coverImage!=null)Row(
                    children: [
                      if(SocialAppCubit.get(context).profileImage!=null)Expanded(child: Column(
                        children: [
                          defaultButton(function: (){
                            SocialAppCubit.get(context).uploadProfileImage(name: nameController.text, phone: phoneController.text, bio: bioController.text);
                          }, text: "upload profile "),
                          if(state is SocialAppUserUpdateLoadingState)SizedBox(height: 5,),
                          if(state is SocialAppUserUpdateLoadingState)LinearProgressIndicator(),
                        ],
                      )),
                      SizedBox(width: 5.0,),
                      if(SocialAppCubit.get(context).coverImage!=null)Expanded(child: Column(
                        children: [
                          defaultButton(function: (){
                            SocialAppCubit.get(context).uploadCoverImage(name: nameController.text, phone: phoneController.text, bio: bioController.text);
                          }, text: "upload cover "),
                          if(state is SocialAppUserUpdateLoadingState)SizedBox(height: 5,),
                          if(state is SocialAppUserUpdateLoadingState)LinearProgressIndicator()
                        ],
                      )),

                    ],
                  ),
                  if(SocialAppCubit.get(context).profileImage!=null||SocialAppCubit.get(context).coverImage!=null)SizedBox(
                    height: 20,
                  ),
                  defaultFromField(
                      controller: nameController,
                      type: TextInputType.name,
                      validate: (String? value) {
                        if (value!.isEmpty) {
                          return "name must not be empty";
                        }
                        return null;
                      },
                      label: "Name ",
                      prefix: Icons.people_alt_outlined),
                  SizedBox(
                    height: 10,
                   ),
                  defaultFromField(
                      controller: bioController,
                      type: TextInputType.name,
                      validate: (String? value) {
                        if (value!.isEmpty) {
                          return "bio must not be empty";
                        }
                        return null;
                      },
                      label: "Bio ",
                      prefix: Icons.info_outlined),
                  SizedBox(
                    height: 10,
                  ),
                  defaultFromField(
                      controller: phoneController,
                      type: TextInputType.phone,
                      validate: (String? value) {
                        if (value!.isEmpty) {
                          return "phone number must not be empty";
                        }
                        return null;
                      },
                      label: "phone ",
                      prefix: Icons.call)
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
