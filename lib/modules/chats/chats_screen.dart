
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialapp/layouts/cubit/cubit.dart';
import 'package:socialapp/layouts/cubit/states.dart';
import 'package:socialapp/models/user_model.dart';
import 'package:socialapp/modules/chat_datails/chat_details_screen.dart';
import 'package:socialapp/shared/components/components.dart';

class ChatsScreen extends StatelessWidget {
  const ChatsScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SocialAppCubit,SocialAppStates>(
      listener: (BuildContext context, Object? state) {},
      builder: (BuildContext context, state) {
        return ConditionalBuilder(
          builder: (BuildContext context) {
            return ListView.separated(
                physics: const BouncingScrollPhysics(),
                itemBuilder: (BuildContext context, int index) => buildChatItem(SocialAppCubit.get(context).users[index],context),
                separatorBuilder: (BuildContext context, int index) => divider(),
                itemCount: SocialAppCubit.get(context).users.length);
          },
          fallback: (BuildContext context){
            return const Center(child: CircularProgressIndicator());
          },
          condition: SocialAppCubit.get(context).users.isNotEmpty,
        );
      },
    );
  }
}

Widget buildChatItem(UserModel user,context) => InkWell(
      onTap:(){
        navigateTo(context,ChatDetailsScreen(userModel: user));
      } ,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Row(
          children: [
            CircleAvatar(
              radius: 25,
              backgroundImage: NetworkImage("${user.image}"),
            ),
            SizedBox(
              width: 15,
            ),
            Text(
              "${user.name}",
              style: TextStyle(height: 1.4),
            ),
          ],
        ),
      ),
    );
