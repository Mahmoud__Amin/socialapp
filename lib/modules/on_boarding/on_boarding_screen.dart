import 'package:flutter/material.dart';
import 'package:socialapp/shared/components/components.dart';
import 'package:socialapp/shared/network/local/cache_helper.dart';
import 'package:socialapp/shared/styles/color.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../login/login_screen.dart';

class PageBoardingModels {
  final String image;
  final String title;
  final String body;

  PageBoardingModels(this.image, this.title, this.body);
}

List<PageBoardingModels> boarding = [
  PageBoardingModels(
      'assets/images/boardingimage1.png', 'On Board1 Title', 'On Board1 Body'),
  PageBoardingModels(
      'assets/images/boardingimage1.png', 'On Board2 Title', 'On Board2 Body'),
  PageBoardingModels(
      'assets/images/boardingimage1.png', 'On Board3 Title', 'On Board3 Body')
];
bool isLast = false;
PageController pageController = PageController();

class OnBoardingScreen extends StatefulWidget {
  @override
  State<OnBoardingScreen> createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {

  void onSubmit(){
    CacheHelper.setData(key: "OnBoarding", value: true);
    navigateReplace(context, LoginScreen());
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          defaultTextButton(
            text: 'skip',
            function:()=>onSubmit()

          )
        ],
      ),
      body: Padding(
        padding:
            const EdgeInsets.only(left: 25, right: 25, bottom: 25, top: 100),
        child: Column(
          children: [
            Expanded(
              child: PageView.builder(
                  itemBuilder: (BuildContext context, int index) =>
                      buildBoardingItem(context, boarding[index]),
                  itemCount: boarding.length,
                  controller: pageController,
                  physics: const BouncingScrollPhysics(),
                  onPageChanged: (int index) {
                    if (index == boarding.length - 1) {
                      setState(() {
                        isLast = true;
                      });
                    } else {
                      setState(() {
                        isLast = false;
                      });
                    }
                  }),
            ),
            Row(
              children: [
                SmoothPageIndicator(
                    controller: pageController,
                    count: boarding.length,
                    effect: CustomizableEffect(
                      activeDotDecoration: DotDecoration(
                          width: 45,
                          height: 14,
                          color: defaultColor,
                          borderRadius: BorderRadius.circular(16)),
                          dotDecoration: DotDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(16),
                          width: 14,
                          height: 14),
                    )),
                const Spacer(),
                FloatingActionButton(
                    child: const Icon(Icons.arrow_forward_ios_outlined),
                    onPressed: () {
                      if (isLast) {
                        onSubmit();
                      } else {
                        pageController.nextPage(
                            duration: const Duration(milliseconds: 750),
                            curve: Curves.fastLinearToSlowEaseIn);
                      }
                    }),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
