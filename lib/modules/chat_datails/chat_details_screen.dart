
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialapp/shared/styles/color.dart';

import '../../layouts/cubit/cubit.dart';
import '../../layouts/cubit/states.dart';
import '../../models/message_model.dart';
import '../../models/user_model.dart';
import '../../shared/components/components.dart';
import '../chats/chats_screen.dart';

class ChatDetailsScreen extends StatelessWidget {
  UserModel? userModel;
  ChatDetailsScreen({
    this.userModel,
});
  var messageController =TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (BuildContext context) {
        SocialAppCubit.get(context).getMessages(receiverId: userModel!.uId!);
        return BlocConsumer<SocialAppCubit,SocialAppStates>(
        listener: (BuildContext context, Object? state) {},
        builder: (BuildContext context, state) {
          return Scaffold(
            appBar: AppBar(
              titleSpacing: 0.0,
              title: Row(
                children: [
                  CircleAvatar(
                    radius: 20.0,
                    backgroundImage: NetworkImage("${userModel?.image}"),
                  )
                  ,SizedBox(
                    width: 15.0,
                  ),
                  Text("${userModel?.name}"),
                ],
              ),
            ),
            body: ConditionalBuilder(
              condition: SocialAppCubit.get(context).messages.isNotEmpty,
              fallback: (BuildContext context) {
                return const Center(child: CircularProgressIndicator());
              },
              builder: (BuildContext context) {
                return Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    Expanded(
                      child: ListView.separated(
                          physics: const BouncingScrollPhysics(),
                          separatorBuilder: (BuildContext context, int index) {
                            return  const SizedBox(height: 15,);
                          },
                          itemBuilder: (BuildContext context, int index) {
                            MessageModel message=SocialAppCubit.get(context).messages[index];
                            if(SocialAppCubit.get(context).userModel?.uId==message.senderId) {
                              return buildMessage(message);
                            }
                            return buildMyMessage(message);
                          },
                          itemCount:SocialAppCubit.get(context).messages.length),
                    ),
                    const SizedBox(height: 15,),
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.grey[300]!,
                            width: 1.0
                        ),
                        borderRadius: BorderRadius.circular(15.0),

                      ),
                      clipBehavior:Clip.antiAliasWithSaveLayer ,
                      child: Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding:EdgeInsets.symmetric(
                                  horizontal: 15
                              ),
                              child: TextFormField(
                                decoration:InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'type your message here ...'
                                ),
                                controller: messageController,
                              ),
                            ),
                          ),
                          MaterialButton(
                            onPressed: (){
                              SocialAppCubit.get(context).sendMessage(receiverId:userModel!.uId!, dateTime: DateTime.now().toString() , text: messageController.text);
                            },
                            minWidth: 1.0,
                            padding: EdgeInsets.zero,
                            child:Container(
                              height: 50,
                              width: 50,
                              color: defaultColor,
                              child: Icon(
                                Icons.send_outlined,
                                size: 16,
                                color: Colors.white,
                              ),
                            ),
                          )
                        ],
                      ),
                    )

                  ],
                ),
              );},
            ),
          );
        },
      );}
    );
  }
  Widget buildMessage(MessageModel message)=>Align(
    alignment: AlignmentDirectional.centerEnd,
    child: Container(
      decoration: BoxDecoration(
          color: defaultColor.withOpacity(0.2),
          borderRadius: BorderRadiusDirectional.only(
            bottomStart : Radius.circular(10.0),
            topStart: Radius.circular(10.0),
            topEnd: Radius.circular(10.0),
          )
      ),
      padding: EdgeInsets.symmetric(
        vertical: 5.0,
        horizontal: 10.0,
      ),
      child:  Text("${message.text}"),
    ),
  );
  Widget buildMyMessage(MessageModel message)=>Align(
    alignment: AlignmentDirectional.centerStart,
    child: Container(
      decoration: BoxDecoration(
          color: Colors.grey[300],
          borderRadius: BorderRadiusDirectional.only(
            bottomEnd: Radius.circular(10.0),
            topStart: Radius.circular(10.0),
            topEnd: Radius.circular(10.0),
          )
      ),
      padding: EdgeInsets.symmetric(
        vertical: 5.0,
        horizontal: 10.0,
      ),
      child: Text("${message.text}"),
    ),
  );

}