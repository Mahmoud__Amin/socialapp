import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socialapp/layouts/cubit/cubit.dart';
import 'package:socialapp/layouts/cubit/states.dart';
import 'package:socialapp/shared/components/components.dart';
class NewPostScreen extends StatelessWidget {

  var textController =TextEditingController();
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SocialAppCubit, SocialAppStates>(
      listener: (BuildContext context, Object? state) {},
      builder: (BuildContext context, state) {
        return Scaffold(
          appBar: defaultAppBar(
              context: context,
              title: "Create Post",
              actions: [
                defaultTextButton(
                function: () {
                var now =DateTime.now();
                if(SocialAppCubit.get(context).postImage ==null){
                  SocialAppCubit.get(context).createPost(dateTime:now.toString(), text: textController.text);
                }
                else{
                  SocialAppCubit.get(context).uploadPostImage(dateTime:now.toString(), text: textController.text);
                }
              }, text: "Post")]),
          body: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                if(state is SocialAppCreatePostLoadingState)LinearProgressIndicator(),
                if(state is SocialAppCreatePostLoadingState)SizedBox(height: 10,),
                Row(
                  children: [
                    CircleAvatar(
                      radius: 20,
                      backgroundImage: NetworkImage("${SocialAppCubit.get(context).userModel?.image}"),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Expanded(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${SocialAppCubit.get(context).userModel?.name}",
                          style: TextStyle(height: 1.4),
                        ),
                      ],
                    )),
                  ],
                ),
                Expanded(
                  child: TextFormField(
                    controller: textController,
                    decoration: InputDecoration(
                        hintText: "what is on your mind",
                        border: InputBorder.none),
                  ),
                ),
                const SizedBox(height: 20,),
                if(SocialAppCubit.get(context).postImage !=null)Stack(
                  alignment: AlignmentDirectional.topEnd,
                  children: [
                    Container(
                        height: 140,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          image: DecorationImage(
                            image: (Image.file(SocialAppCubit.get(context).postImage!).image),
                            fit: BoxFit.cover,
                          ),
                        )),
                    IconButton(
                        onPressed: () {
                          SocialAppCubit.get(context).removePostImage();
                        },
                        icon: CircleAvatar(
                            radius: 20.0,
                            child: Icon(
                              Icons.close,
                              size: 16,
                            )))
                  ],
                ),
                const SizedBox(height: 20,),
                Row(
                  children: [
                    Expanded(
                      child: TextButton(
                          onPressed: () {
                            SocialAppCubit.get(context).getPostImage();
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.image_outlined),
                              SizedBox(
                                width: 5,
                              ),
                              Text("add photo")
                            ],
                          )),
                    ),
                    Expanded(
                      child:
                          TextButton(onPressed: () {}, child: Text("# tags")),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
